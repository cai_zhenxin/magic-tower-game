#pragma once
#include "Goods.h"

class Bag
{
public:
	Bag();
	~Bag();

private:
	Goods* items[40];			//物品数组
	int size;					//背包容量
	int goodsNum;				//当前物品数量
public:
	//获取物品
	Goods* getGoods(int i);
	//将拾取的物品添加到背包中
	void setGoods(int i, Goods goods);
	

	//设置物品
	void setNullptr(int i);
	void chageGoods(int i);

	//获取内存
	void getMemory(int i);	

	//设置物品地址
	void setPitems(int i, Goods* pGoods);

	//获取背包容量
	int getSize();

	//获取当前物品数量
	int getGoodsNum();

	//设置背包容量
	void setSize(int s);

	//设置当前物品数量
	void setGoodsNum(int num);
};

