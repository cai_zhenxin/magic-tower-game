#pragma once
#include "Game.h"
#include "Monster.h"
#include "Hero.h"
#include "Map.h"
#include <graphics.h>
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include "mmsystem.h"
#pragma comment(lib,"winmm.lib")
using namespace std;

//定义IMAGE类型变量
IMAGE imgproSection;
IMAGE imgUI;
IMAGE imgGameHelp;
IMAGE imgEnd;
IMAGE imgStory;

Game::Game()
{
	this->nowLevel = 0;
	this->isTalk = 0;
	this->isDemonTalk = 0;
	for (int i = 0; i < 20; i++)
	{
		this->isPlay[i] = 0;
	}
}

Game::~Game()
{
}

//设置当前层数
void Game::setNowLevel(int level)
{
	this->nowLevel = level;
}

//设置当前层数
void Game::initIsPlay()
{
	for (int i = 0; i < 20; i++)
	{
		this->isPlay[i] = 0;
	}
}

//获取当前仙子是否已经跟英雄对话过的bool值
bool Game::getIsTalk()
{
	return this->isTalk;
}

// 获取当前魔王是否已经跟英雄对话过的bool值
bool Game::getIsDemonTalk()
{
	return this->isDemonTalk;
}

//设置当前仙子与英雄的对话状态
void Game::setIsTalk(bool b)
{
	this->isTalk = b;
}


//设置当前魔王与英雄的对话状态
void Game::setIsDemonTalk(bool b)
{
	this->isDemonTalk = b;
}
//初始化
void Game::init(Map& map, Hero& hero)
{
	map.getMemory();     //获取内存
	map.initMap();		//初始化地图
	map.getHeroPos(hero);	//获取英雄坐标
}

//绘制属性栏
void Game::drawTag(Map& map, Hero& hero)
{
	//加载属性栏背景图片
	loadimage(&imgproSection, "Picture/Tag.png", 255, 650);
	//int i = 0;
	//定义并初始化各种钥匙的数量
	int yellowKeyNum = 0, blueKeyNum = 0, redKeyNum = 0;
	for (int i = 0; i < hero.getBag().getSize(); i++)
	{
		if (hero.getBag().getGoods(i) != nullptr)
		{
			if (hero.getBag().getGoods(i)->id == 1007)
			{
				yellowKeyNum++;
			}
			if (hero.getBag().getGoods(i)->id == 1006)
			{
				blueKeyNum++;
			}
			if (hero.getBag().getGoods(i)->id == 1005)
			{
				redKeyNum++;
			}
		}
		else
		{
			continue;
		}

	}
	//在右边添加属性栏背景
	putimage(650, 0, &imgproSection);
	// 在屏幕右边输出字符串
	char tag[30];
	setbkmode(TRANSPARENT);
	settextstyle(20, 20,tag);

	sprintf(tag, "%d", hero.getLv());
	outtextxy(780, 80, tag);
	
	sprintf(tag, "%d", hero.getHP());
	outtextxy(750, 130, tag);

	sprintf(tag, "%d", hero.getATK());
	outtextxy(750, 165, tag);

	sprintf(tag, "%d", hero.getDEF());
	outtextxy(750, 200, tag);

	sprintf(tag, "%d", hero.getMoney());
	outtextxy(750, 240, tag);

	sprintf(tag, "%d", hero.getExp());
	outtextxy(750, 280, tag);

	sprintf(tag, "%d", yellowKeyNum);
	outtextxy(780, 355, tag);

	sprintf(tag, "%d", blueKeyNum);
	outtextxy(780, 405, tag);

	sprintf(tag, "%d", redKeyNum);
	outtextxy(780, 455, tag);

	settextstyle(20, 10, tag);
	sprintf(tag, "%d", hero.getLevel());
	outtextxy(760, 505, tag);

}

//绘制
void Game::draw(Map& map, Hero& hero)
{
	//清屏
	system("cls"); 
	//绘制地图
	map.drawMap(hero);
	//绘制属性栏
	drawTag(map, hero);
}

//通往下一关
void Game::nextLevel(Map& map, Hero& hero, int x)
{
	//n为当前层数
	//static int n = 0;
	// change为当前关卡有无游玩的判断
	//static int change[20] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	//存储当前关卡
	char fileName[30];
	//存储当前关卡为beforeGoMap地图
	sprintf(fileName, "Map/beforeGoMap_%d.txt", nowLevel);
	map.saveFile(fileName);
	//当前关卡变为游玩状态
	isPlay[nowLevel] = 1;
	//到下一关卡
	nowLevel += x;
	//读取下一关卡
	//当下一关卡是已经玩过的 读取beforeGoMap地图
	if (1 == isPlay[nowLevel])
	{
		//给字符数组赋值
		sprintf(fileName, "Map/beforeGoMap_%d.txt", nowLevel);
		//调用函数读取地图文件
		map.readFile(fileName);
	}
	//当下一关卡不是玩过的 读取map地图
	else if (0 == isPlay[nowLevel])
	{
		//给字符数组赋值
		sprintf(fileName, "Map/map_%d.txt", nowLevel);
		//调用函数读取地图文件
		map.readFile(fileName);
	}
	//英雄当前所在层数改变
	hero.setLevel(nowLevel);

}

//清除消息
void Game::ClearMessage()
{
	//设置填充色为黑色
	setfillcolor(BLACK);
	//绘制一个无边框的填充矩形
	solidrectangle(0, 650, 900, 750);
}

//绘制对话框
void Game::drawDialog(int x, int y, int width, int height, char *str)
{
	//设置填充颜色为黑色
	setfillcolor(BLACK);
	//定义一个有边框的填充矩形
	fillrectangle(x, y, x + width, y + height);
	//设置文字背景色为透明
	setbkmode(TRANSPARENT);
	//定义并初始化一个矩形区域
	RECT r1 = { x, y, x + width, y + height };
	//设置文字类型
	settextstyle(20, 0, _T(""));
	//设置文字颜色
	settextcolor(WHITE);
	//绘制文字
	drawtext(str, &r1, DT_WORDBREAK);
}

//在指定矩形区域输出文字
void Game::printText(int num,char* str)
{
	//定义并初始化一个矩形区域
	RECT r1 = { 120, 50*num, 650, 150 * num };
	//设置文字类型
	settextstyle(20, 0, _T(""));
	//设置文字颜色
	settextcolor(WHITE);
	//绘制文字
	drawtext(str, &r1, DT_WORDBREAK);
}

//在指定位置输出图片
void Game::printImage(int num, char* monsterName)
{
	//定义一个IMAGE变量
	IMAGE imgMonster;
	//定义一个char型数组，用于存储图片路径
	char fileName[50];
	//给字符数组赋值
	sprintf(fileName, "Picture/%s.png", monsterName);
	//加载对应图片
	loadimage(&imgMonster, fileName, 50, 50);
	//在当前窗口的对应位置添加图片
	putimage(50, 50 * num, &imgMonster);
}

//定义一个函数计算当前英雄与某一怪物战斗所需要损耗的生命值
char* Game::calculateLoss(Hero hero, Monster monster)
{
	if (hero.getATK() <= monster.getDEF())	//当英雄的攻击小于等于怪物的防御时
	{
		return "???";	//返回
	}
	if (hero.getDEF() >= monster.getATK())	//当英雄的防御大于等于怪物的攻击时
	{
		return "0";	//返回
	}
	int monsterHP = monster.getHP();
	int hurt_Hero = hero.getATK() - monster.getDEF();		//定义一个局部变量表示每一回合英雄对怪物造成的伤害
	int hurt_Monster = monster.getATK() - hero.getDEF();		//定义一个局部变量表示每一回合怪物对英雄造成的伤害
	int time = 0;		//定义一个局部变量表示回合数
	while (monsterHP>0)		//当怪物的血量大于0时
	{
		monsterHP -= hurt_Hero;	//每一回合怪物的血量减少
		time++;		//回合数+1
	}
	int loss = hurt_Monster*time;	//定义一个int型变量接收英雄受到的伤害
	char strLoss[10];	//定义一个char型数组
	sprintf(strLoss, "%d", loss);	//将int型数据转换成char*型
	return strLoss;	//返回
}

//定义一个函数判断数组中是否已经有某一元素
bool Game::isExist(int* monsterList, int id)
{
	for (int k = 0; k < 10; k++)
	{
		if (monsterList[k] == id)
		{
			return true;
		}
	}
	return false;
}

//定义一个函数输出指定怪物的信息
void Game::monsterData(Monster monster, Hero& hero, int* monsterType, int& num, char* monsterNmae)
{
	//定义一个字符数组表示怪物信息
	char monsterInformation[100];
	//将怪物种类添加到数组中，以便排除重复
	monsterType[num] = monster.getID();
	//怪物列表中的怪物种类+1
	num++;	
	//输出怪物图片
	printImage(num, monsterNmae);
	//编辑输出的文字
	sprintf(monsterInformation, "名称：%s   攻击：%d   金·经：%d·%d\n生命：%d       防御：%d     损失：%s", monster.getName(), monster.getATK(), monster.getExp(), monster.getMoney(), monster.getHP(), monster.getDEF(), calculateLoss(hero, monster));
	//调用函数在指定位置输出文字
	printText(num, monsterInformation);
}

//定义一个播放拾取物品音效的函数
void Game:: playSound()
{
	//打开音乐文件
	mciSendString("open Music\\getGoods.wav", NULL, 0, NULL);
	//播放音乐文件	
	mciSendString("play Music\\getGoods.wav", NULL, 0, NULL);
	Sleep(300);
	//关闭音乐文件	
	mciSendString("close Music\\getGoods.wav", NULL, 0, NULL);
}

//定义一个游戏UI界面函数
void Game::UI()
{
	//绘图窗口初始化
	initgraph(900, 750);
	//加载游戏主界面背景图片
	loadimage(&imgUI, "Picture/UI.png", 900, 750);
	//加载背景故事的图片
	loadimage(&imgStory, "Picture/story.png", 900, 650);
	//在窗口放置加载好的图片
	putimage(0, 0, &imgUI);
	//绘制“开始游戏”的按钮
	rectangle(350, 380, 550, 450);
	//绘制“游戏说明”的按钮
	rectangle(350, 480, 550, 550);
	//绘制“离开游戏”的按钮
	rectangle(350, 580, 550, 650);
	//定义一个MOUSEMSG类型的变量
	MOUSEMSG msg;
	while (true)
	{
		while (MouseHit())// 当有鼠标消息的时候执行
		{
			//获取鼠标消息
			msg = GetMouseMsg();//
			//如果鼠标消息为左键按下
			if (msg.uMsg == WM_LBUTTONDOWN)
			{
				//点击“开始游戏”按钮时
				if (judgeButton(msg.x, msg.y, 350, 380))
				{
					//在窗口放置加载好的图片
					putimage(0, 0, &imgStory);
					_getch();
					//退出函数
					return;
				}
				//点击“游戏说明”按钮时
				if (judgeButton(msg.x, msg.y, 350, 480))
				{
					//调用游戏说明函数
					gameHelp();
				}
				//点击“离开游戏”按钮时
				if (judgeButton(msg.x, msg.y, 350, 580))
				{
					//退出程序
					exit(0);
				}
			}
		}
	}
	
}

//定义一个游戏说明界面函数
void Game::gameHelp()
{
	//加载游戏说明背景图片
	loadimage(&imgGameHelp, "Picture/gameHelp.png", 900, 750);
	//把加载好的图片放在窗口
	putimage(0, 0, &imgGameHelp);
	_getch();
	loadimage(&imgUI, "Picture/UI2.png", 900, 750);
	//在窗口放置加载好的图片
	putimage(0, 0, &imgUI);
}

//定义一个游戏结束界面函数
void Game::end(Hero& hero,Map& map)
{
	//加载游戏主界面背景图片
	loadimage(&imgEnd, "Picture/end.png", 900, 750);
	//在窗口放置加载好的图片
	putimage(0, 0, &imgEnd);
	char choose = _getch();
	while (true)
	{
		if (choose == 'R' || choose == 'r')
		{
			break;
		}
		else if (choose == 'Q' || choose == 'q')
		{
			//退出程序
			exit(0);
		}
	}
	hero.initHero();
	initIsPlay();
	setIsTalk(0);
	setIsDemonTalk(0);
	setNowLevel(0);
	map.readFile("Map/map_0.txt");
}

//判断点(x,y)是不是在按钮(bx,by)范围内
bool Game::judgeButton(int x, int y, int bx, int by)
{
	//如果鼠标点击的坐标位于此范围
	if (x >= bx&&x <= bx + 200&&y >= by&&y <= by + 70)
		//返回真
		return true;
	//否则返回假
	return false;
}

//定义一个重新开始游戏的功能函数
void Game::restart(Map& map,Hero& hero)
{
	drawDialog(100, 300, 450, 250, "\n            是否确定重新开始游戏？\n\n\n\n     是(Y/y)            否(N/n)");
	char choose = _getch();
	while (true)
	{
		if (choose == 'Y' || choose == 'y')
		{
			break;
		}
		else if (choose == 'N' || choose == 'n')
		{
			return;
		}
	}
	hero.initHero();
	initIsPlay();
	setIsTalk(0);
	setNowLevel(0);
	map.readFile("Map/map_0.txt");
}

//定义一个退出游戏的功能函数
void Game::exitGame()
{
	drawDialog(100, 300, 450, 250, "\n            是否确定退出游戏？\n\n\n\n     是(Y/y)             否(N/n)");
	char choose = _getch();
	while (true)
	{
		if (choose == 'Y' || choose == 'y')
		{
			exit(0);
		}
		else if (choose == 'N' || choose == 'n')
		{
			return;
		}
	}
}