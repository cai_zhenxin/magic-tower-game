#include "Monster.h"

//带参构造函数
Monster::Monster(int num,char* name,int HP,int atk,int def,int money,int exp)
{
	this->num = num;
	this->name = name;
	this->HP = HP;
	this->atk = atk;
	this->def = def;
	this->money = money;
	this->exp = exp;
}

Monster::~Monster()
{
}

//获取怪物编号
int Monster::getID()
{
	return this->num;
}

// 获取怪物名称
char* Monster::getName()
{
	return this->name;
}

//获取怪物血量
int Monster::getHP()
{
	return this->HP;
}
//获取怪物攻击力
int Monster::getATK()
{
	return this->atk;
}
//获取怪物防御力
int Monster::getDEF()
{
	return this->def;
}
//获取击杀金币数
int Monster::getMoney()
{
	return this->money;
}

//获取击杀经验值
int Monster::getExp()
{
	return this->exp;
}

