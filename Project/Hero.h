#pragma once
#include "Bag.h"
class Map;
class Monster;
class Game;

// 英雄方向状态的枚举
enum Direction
{
	Up,			//向上
	Right,		//向右
	Down,		//向下
	Left		//向左
};

//英雄类
class Hero
{
public:
	Hero(int Lv, int HP, int atk, int def, int money, int exp, int level, int posx, int posy, Direction direction);
	~Hero();

private:
	int Lv;	//等级
	int HP;	//血量
	int atk;	//攻击力
	int def;	//防御力
	int money;	//金币
	int level;// 英雄所在地图的层数
	int exp;// 英雄经验值
	int posx;  //英雄x坐标
	int posy;  //英雄y坐标
	Direction direction;   //方向状态
	Bag bag;	//背包
public:
	//获取英雄等级
	int getLv();

	//获取英雄血量
	int getHP();

	//获取英雄攻击力
	int getATK();

	//获取英雄防御力
	int getDEF();

	//获取英雄所持金币数
	int getMoney();

	//获取当前英雄经验值
	int getExp();

	//获取英雄x坐标
	int getPosx();

	//获取英雄y坐标
	int getPosy();

	//获取英雄所在层数
	int getLevel();

	//获取英雄方向状态
	Direction getDirection();

	//给英雄的背包的第i个物品申请内存
	void getMemory(int i);

	//使用物品后把对应的物品指针置空
	void Hero::setNullptr(int i);

	//获取背包
	Bag getBag();

	//初始化英雄属性
	void initHero();

	//修改x坐标
	void setPosx(int x);

	//修改y坐标
	void setPosy(int y);

	//修改英雄方向
	void setDirection(Direction);

	//修改英雄所在层数
	void setLevel(int n);

	//移动方法
	void move(Map& map, Hero& hero, Game& game, int X, int Y);

	//声明一个判断背包中特定物品是否存在的函数
	int selectGoods(int id);

	//遇见怪物
	void meetMonster(Map& map, Monster monster, Game& game, int X, int Y);

	//战斗
	bool battle( Monster monster,Game& game);

	//拾取物品
	void getGoods(int id);

	//使用洞察之眼
	void detectionMonster(Hero& hero,Map& map, Game& game);
};
