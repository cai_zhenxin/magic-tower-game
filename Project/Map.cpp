#include "Map.h"
#include "Game.h"
#include "Hero.h"
#include <iostream>
#include <conio.h>
using namespace std;

//地形图片
IMAGE imgFloor;
IMAGE imgWall;
IMAGE imgPurpleWall;
IMAGE imgYellowDoor;
IMAGE imgBlueDoor;
IMAGE imgRedDoor;
IMAGE imgGreenDoor;
IMAGE imgIronDoor;
IMAGE imgUpstairs;
IMAGE imgDownstairs;
IMAGE imgMagma;
IMAGE imgStar;

//英雄图片
IMAGE imgHeroDown;
IMAGE imgHeroUp;
IMAGE imgHeroLeft;
IMAGE imgHeroRight;

//道具图片
IMAGE imgRedKey;
IMAGE imgBlueKey;
IMAGE imgYellowKey;
IMAGE imgInsightEye;
IMAGE imgCross;

//怪物图片
IMAGE imgGreenSlime;
IMAGE imgRedSlime;
IMAGE imgBlackSlime;
IMAGE imgSmallBat;
IMAGE imgSkeletonMan;
IMAGE imgSkeletonSoilder;
IMAGE imgPrimaryMage;
IMAGE imgBigBat;
IMAGE imgBeastMan;
IMAGE imgSkeletonCaptain;
IMAGE imgSackclothMage;
IMAGE imgStoneMan;
IMAGE imgPrimaryGuard;
IMAGE imgRedBat;
IMAGE imgHightMage;
IMAGE imgMonsterKing;
IMAGE imgWhiteKnight;
IMAGE imgGoldGuard;
IMAGE imgRedMage;
IMAGE imgBeastKnight;
IMAGE imgDarkGuard;
IMAGE imgHightGuard;
IMAGE imgSwordMan;
IMAGE imgDarkWarrior;
IMAGE imgGoldCaptain;
IMAGE imgSoulMage;
IMAGE imgDarkCaptain;
IMAGE imgSoulKnight;
IMAGE imgShadowWarrior;
IMAGE imgRedDemon;
IMAGE imgDemonKing;

//增益物品图片
IMAGE imgRedDrug;
IMAGE imgBlueDrug;
IMAGE imgRedGem;
IMAGE imgBlueGem;
IMAGE imgSword;
IMAGE imgBetterSword;
IMAGE imgShield;
IMAGE imgBetterShield;
IMAGE imgBestShield;
IMAGE imgBestSword;
IMAGE imgBetterKey;
IMAGE imgGold;
IMAGE imgHexagram;
IMAGE imgHolyBottle;

//NPC图片
IMAGE imgBusinessman;
IMAGE imgOldman;
IMAGE imgThief;
IMAGE imgBlueShop; 
IMAGE imgFaery;
IMAGE imgRedShop;
IMAGE imgProfiteer;
IMAGE imgImmortal;
IMAGE imgPrincess;

//带参构造函数
Map::Map(int** pMap, int row, int col)
{
	this->pMap = pMap;
	this->row = row;
	this->col = col;
}

Map::~Map()
{
}

//获取地图首地址
int** Map::getPmap()
{
	return this->pMap;
}

//获取地图上指定坐标的数字
int Map::getPosNumber(Hero& hero, int x, int y)
{
	return pMap[hero.getPosx() + x][hero.getPosy() + y];
}

//设置地图上指定坐标的数字
void Map::setPosNumber(Hero& hero, int x, int y,int val)
{
	pMap[hero.getPosx() + x][hero.getPosy() + y] = val;
}

//设置地图上指定坐标的数字
void Map::setPosNumber(Hero& hero,int val)
{
	pMap[hero.getPosx()][hero.getPosy()] = val;
}

///获取内存
void Map::getMemory()
{
	pMap = new int*[row];
	for (int i = 0; i < col; i++)
	{
		pMap[i] = new int[col];
	}
}

//释放内存
void Map::deleteMemory()
{
	//先释放每一行的内存
	for (int j = 0; j < row; j++)
	{
		delete[] pMap[j];
		pMap[j] = nullptr;
	}
	//再释放数组首地址的内存
	delete[]  pMap;
	pMap = nullptr;
}

//获取英雄坐标
void Map::getHeroPos(Hero& hero)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (pMap[i][j] == 2)
			{
				hero.setPosx(i);
				hero.setPosy(j);
				return;
			}
		}
	}
}

//初始化地图
void Map::initMap()
{
	//英雄
	loadimage(&imgHeroDown, "Picture/heroDown.png", 50, 50);
	loadimage(&imgHeroUp, "Picture/heroUp.png", 50, 50);
	loadimage(&imgHeroLeft, "Picture/heroLeft.png", 50, 50);
	loadimage(&imgHeroRight, "Picture/heroRight.png", 50, 50);
	
	//地形
	loadimage(&imgFloor, "Picture/floor.png", 50, 50);
	loadimage(&imgWall, "Picture/wall.png", 50, 50);
	loadimage(&imgPurpleWall, "Picture/purpleWall.png", 50, 50);
	loadimage(&imgYellowDoor, "Picture/yellowDoor.png", 50, 50);
	loadimage(&imgBlueDoor, "Picture/blueDoor.png", 50, 50);
	loadimage(&imgRedDoor, "Picture/redDoor.png", 50, 50);
	loadimage(&imgGreenDoor, "Picture/greenDoor.png", 50, 50);
	loadimage(&imgIronDoor, "Picture/ironDoor.png", 50, 50);
	loadimage(&imgUpstairs, "Picture/upstairs.png", 50, 50);
	loadimage(&imgDownstairs, "Picture/downstairs.png", 50, 50);
	loadimage(&imgMagma, "Picture/magma.png", 50, 50);
	loadimage(&imgStar, "Picture/star.png", 50, 50);

	//道具
	loadimage(&imgRedKey, "Picture/redKey.png", 50, 50);
	loadimage(&imgBlueKey, "Picture/blueKey.png", 50, 50);
	loadimage(&imgYellowKey, "Picture/yellowKey.png", 50, 50);
	loadimage(&imgInsightEye, "Picture/insightEye.png", 50, 50);
	loadimage(&imgCross, "Picture/cross.png", 50, 50);
	loadimage(&imgHolyBottle, "Picture/holyBottle.png", 50, 50);
	
	//怪物
	loadimage(&imgGreenSlime, "Picture/greenSlime.png", 50, 50);
	loadimage(&imgRedSlime, "Picture/redSlime.png", 50, 50);
	loadimage(&imgBlackSlime, "Picture/blackSlime.png", 50, 50);
	loadimage(&imgSmallBat, "Picture/smallBat.png", 50, 50);
	loadimage(&imgSkeletonMan, "Picture/skeletonMan.png", 50, 50);
	loadimage(&imgSkeletonSoilder, "Picture/skeletonSoilder.png", 50, 50);
	loadimage(&imgPrimaryMage, "Picture/primaryMage.png", 50, 50);
	loadimage(&imgBigBat, "Picture/bigBat.png", 50, 50);
	loadimage(&imgBeastMan, "Picture/beastMan.png", 50, 50);
	loadimage(&imgSkeletonCaptain, "Picture/skeletonCaptain.png", 50, 50);
	loadimage(&imgStoneMan, "Picture/stoneMan.png", 50, 50);	
	loadimage(&imgSackclothMage, "Picture/sackclothMage.png", 50, 50);
	loadimage(&imgPrimaryGuard, "Picture/primaryGuard.png", 50, 50);
	loadimage(&imgRedBat, "Picture/redBat.png", 50, 50);
	loadimage(&imgHightMage, "Picture/hightMage.png", 50, 50);
	loadimage(&imgMonsterKing, "Picture/monsterKing.png", 50, 50);
	loadimage(&imgWhiteKnight, "Picture/whiteKnight.png", 50, 50);
	loadimage(&imgGoldGuard, "Picture/goldGuard.png", 50, 50);
	loadimage(&imgRedMage, "Picture/redMage.png", 50, 50);
	loadimage(&imgBeastKnight, "Picture/beastKnight.png", 50, 50);
	loadimage(&imgDarkGuard, "Picture/darkGuard.png", 50, 50);
	loadimage(&imgHightGuard, "Picture/hightGuard.png", 50, 50);
	loadimage(&imgSwordMan, "Picture/swordMan.png", 50, 50);
	loadimage(&imgDarkWarrior, "Picture/darkWarrior.png", 50, 50);
	loadimage(&imgGoldCaptain, "Picture/goldCaptain.png", 50, 50);
	loadimage(&imgSoulMage, "Picture/soulMage.png", 50, 50);
	loadimage(&imgDarkCaptain, "Picture/darkCaptain.png", 50, 50);
	loadimage(&imgSoulKnight, "Picture/soulKnight.png", 50, 50);
	loadimage(&imgShadowWarrior, "Picture/shadowWarrior.png", 50, 50);
	loadimage(&imgRedDemon, "Picture/redDemon.png", 50, 50);
	loadimage(&imgDemonKing, "Picture/demonKing.png", 50, 50);
	
	//增益物品
	loadimage(&imgRedDrug, "Picture/redDrug.png", 50, 50);
	loadimage(&imgBlueDrug, "Picture/blueDrug.png", 50, 50);
	loadimage(&imgRedGem, "Picture/redGem.png", 50, 50);
	loadimage(&imgBlueGem, "Picture/blueGem.png", 50, 50); 
	loadimage(&imgSword, "Picture/sword.png", 50, 50);
	loadimage(&imgGold, "Picture/gold.png", 50, 50);
	loadimage(&imgHexagram, "Picture/hexagram.png", 50, 50);
	loadimage(&imgShield, "Picture/shield.png", 50, 50);
	loadimage(&imgBetterKey, "Picture/betterKey.png", 50, 50);
	loadimage(&imgBetterSword, "Picture/betterSword.png", 50, 50);
	loadimage(&imgBetterShield, "Picture/betterShield.png", 50, 50);
	loadimage(&imgBestSword, "Picture/bestSword.png", 50, 50);
	loadimage(&imgBestShield, "Picture/bestShield.png", 50, 50);

	//加载NPC
	loadimage(&imgBusinessman, "Picture/businessman.png", 50, 50);
	loadimage(&imgOldman, "Picture/oldman.png", 50, 50);
	loadimage(&imgThief, "Picture/thief.png", 50, 50);
	loadimage(&imgBlueShop, "Picture/blueShop.png", 50, 50);
	loadimage(&imgFaery, "Picture/faery.png", 50, 50);
	loadimage(&imgRedShop, "Picture/redShop.png", 50, 50);
	loadimage(&imgProfiteer, "Picture/profiteer.png", 50, 50);
	loadimage(&imgImmortal, "Picture/immortal.png", 50, 50);
	loadimage(&imgPrincess, "Picture/princess.png", 50, 50);

	//读取地图文件
	readFile("Map/map_0.txt");
}

void Map::drawMap(Hero& hero)
{

	//贴图
	for (int i = 0; i < row; i++)//行
	{
		for (int j = 0; j < col; j++)//列
		{
			switch (pMap[i][j])
			{
			case -3:
				putimage(j * 50, i * 50, &imgStar); 
				break;
			case -2:
				putimage(j * 50, i * 50, &imgMagma);
				break;
			
			case 0:
				//setColor(267);
				//cout << "  "; //空地 □ ☆ ★ ※
				putimage(j * 50, i * 50, &imgFloor);
				break;
			case 1:
				//setColor(271);
				//cout << "█";  //外墙
				putimage(j * 50, i * 50, &imgPurpleWall);
				break;
			case 9: 
				//内墙
				putimage(j * 50, i * 50, &imgWall);
				break;
			case 2:
				//setColor(269);
				//cout << "♀"; //英雄
				if (hero.getDirection() == Down)
				{
					putimage(j * 50, i * 50, &imgHeroDown);
				}
				else if (hero.getDirection() == Up)
				{
					putimage(j * 50, i * 50, &imgHeroUp);
				}
				else if (hero.getDirection() == Left)
				{
					putimage(j * 50, i * 50, &imgHeroLeft);
				}
				else if (hero.getDirection() == Right)
				{
					putimage(j * 50, i * 50, &imgHeroRight);
				}

				break;
			case 3:
				//setColor(260);
				//cout << "★"; //红钥匙
				putimage(j * 50, i * 50, &imgRedKey);
				break;
			case 4:
				//setColor(265);
				//cout << "★"; //蓝钥匙
				putimage(j * 50, i * 50, &imgBlueKey);
				break;
			case 5:
				//setColor(270);
				//cout << "★"; //黄钥匙
				putimage(j * 50, i * 50, &imgYellowKey);
				break;
			case 13:
				//setColor(260);
				//cout << "█"; //红门
				putimage(j * 50, i * 50, &imgRedDoor);
				break;
			case 14:
				//setColor(265);
				//cout << "█"; //蓝门
				putimage(j * 50, i * 50, &imgBlueDoor);
				break;
			case 15:
				//setColor(270);
				//cout << "█"; //黄门
				putimage(j * 50, i * 50, &imgYellowDoor);
				break;
			case 16:
				//绿门
				putimage(j * 50, i * 50, &imgGreenDoor);
				break;
			case 17:
				//铁门
				putimage(j * 50, i * 50, &imgIronDoor);
				break;

			case 10:
				//setColor(10);
				//cout << "▲"; //上楼
				putimage(j * 50, i * 50, &imgUpstairs);
				break;
			case 11:
				//setColor(10);
				//cout << "▼"; //下楼
				putimage(j * 50, i * 50, &imgDownstairs);
				break;

				//怪物
			case 20:
				//绿色史莱姆
				putimage(j * 50, i * 50, &imgGreenSlime);
				break;
			case 21:
				//红色史莱姆
				putimage(j * 50, i * 50, &imgRedSlime);
				break;
			case 22:
				//小蝙蝠
				putimage(j * 50, i * 50, &imgSmallBat);
				break;
			case 23:
				//骷髅人
				putimage(j * 50, i * 50, &imgSkeletonMan);
				break;
			case 24:
				//青头怪
				putimage(j * 50, i * 50, &imgBlackSlime);
				break;
			case 25:
				//骷髅士兵
				putimage(j * 50, i * 50, &imgSkeletonSoilder);
				break;
			case 26:
				//初级法师
				putimage(j * 50, i * 50, &imgPrimaryMage);
				break;
			case 27:
				//大蝙蝠
				putimage(j * 50, i * 50, &imgBigBat);
				break;
			case 28:
				//兽面人
				putimage(j * 50, i * 50, &imgBeastMan);
				break;
			case 29:
				//骷髅队长
				putimage(j * 50, i * 50, &imgSkeletonCaptain);
				break;
			case 30:
				//麻衣法师
				putimage(j * 50, i * 50, &imgSackclothMage);
				break;
			case 31:
				//初级卫兵
				putimage(j * 50, i * 50, &imgPrimaryGuard);
				break;
			case 32:
				//红蝙蝠
				putimage(j * 50, i * 50, &imgRedBat);
				break;
			case 33:
				//高级法师
				putimage(j * 50, i * 50, &imgHightMage);
				break;
			case 34:
				//怪王
				putimage(j * 50, i * 50, &imgMonsterKing);
				break;
			case 35:
				//白衣武士
				putimage(j * 50, i * 50, &imgWhiteKnight);
				break;
			case 36:
				//金卫士
				putimage(j * 50, i * 50, &imgGoldGuard);
				break;
			case 37:
				//红衣法师
				putimage(j * 50, i * 50, &imgRedMage);
				break;
			case 38:
				//兽面武士
				putimage(j * 50, i * 50, &imgBeastKnight);
				break;
			case 39:
				//冥卫兵
				putimage(j * 50, i * 50, &imgDarkGuard);
				break;
			case 40:
				//高级卫兵
				putimage(j * 50, i * 50, &imgHightGuard);
				break;
			case 41:
				//双手剑士
				putimage(j * 50, i * 50, &imgSwordMan);
				break;
			case 42:
				//冥战士
				putimage(j * 50, i * 50, &imgDarkWarrior);
				break;
			case 43:
				//金队长
				putimage(j * 50, i * 50, &imgGoldCaptain);
				break;
			case 44:
				//灵法师
				putimage(j * 50, i * 50, &imgSoulMage);
				break;
			case 45:
				//冥队长
				putimage(j * 50, i * 50, &imgDarkCaptain);
				break;
			case 46:
				//灵战士
				putimage(j * 50, i * 50, &imgSoulKnight);
				break;
			case 47:
				//影子战士
				putimage(j * 50, i * 50, &imgShadowWarrior);
				break;
			case 48:
				//石头怪人
				putimage(j * 50, i * 50, &imgStoneMan);
				break;

				//增益物品
			case 50:
				//红药水
				putimage(j * 50, i * 50, &imgRedDrug);
				break;
			case 51:
				//蓝药水
				putimage(j * 50, i * 50, &imgBlueDrug);
				break;
			case 52:
				//红宝石
				putimage(j * 50, i * 50, &imgRedGem);
				break;
			case 53:
				//蓝宝石
				putimage(j * 50, i * 50, &imgBlueGem);
				break;
			case 54:
				//铁剑
				putimage(j * 50, i * 50, &imgSword);
				break;
			case 55:
				//铁盾
				putimage(j * 50, i * 50, &imgShield);
				break;
			case 56:
				//钥匙盒
				putimage(j * 50, i * 50, &imgBetterKey);
				break;

			case 57:
				//大金币
				putimage(j * 50, i * 50, &imgGold);
				break;
			case 58:
				//六芒星
				putimage(j * 50, i * 50, &imgHexagram);
				break;
				
			case 59:
				//洞察之眼
				putimage(j * 50, i * 50, &imgInsightEye);
				break;
			case 60:
				//幸运十字架
				putimage(j * 50, i * 50, &imgCross);
				break;
			case 61:
				//朱雀剑
				putimage(j * 50, i * 50, &imgBetterSword);
				break;
			case 62:
				//玄武盾
				putimage(j * 50, i * 50, &imgBetterShield);
				break;
			case 63:
				//圣光瓶
				putimage(j * 50, i * 50, &imgHolyBottle);
				break;
			case 64:
				//屠魔剑
				putimage(j * 50, i * 50, &imgBestSword);
				break;
			case 65:
				//镇魔盾
				putimage(j * 50, i * 50, &imgBestShield);
				break;

				//NPC
			case 70:
				//商人
				putimage(j * 50, i * 50, &imgBusinessman);
				break;
			case 71:
				//老人
				putimage(j * 50, i * 50, &imgOldman);
				break;
			case 72:
				//小偷
				putimage(j * 50, i * 50, &imgThief);
				break;
			case 73:
				//蓝商店
				putimage(j * 50, i * 50, &imgBlueShop);
				break;
			case 74:
				//红商店
				putimage(j * 50, i * 50, &imgRedShop);
				break;
			case 75:
				//奸商
				putimage(j * 50, i * 50, &imgProfiteer);
				break;
			case 76:
				//仙人
				putimage(j * 50, i * 50, &imgImmortal);
				break;
			case 77:
				//公主
				putimage(j * 50, i * 50, &imgPrincess);
				break;
			case 79:
				//仙子
				putimage(j * 50, i * 50, &imgFaery);
				break;
			case 80:
				//红衣魔王
				putimage(j * 50, i * 50, &imgRedDemon);
				break;
			case 81:
				//冥灵魔王
				putimage(j * 50, i * 50, &imgDemonKing);
				break;
				
			default:
				break;
			}
		}
		cout << endl;
	}
}

//更新
void Map::update(Map& map, Hero& hero,Game& game)
{
	getHeroPos(hero);
	switch (_getch())
	{
		//上移
		case 'w': 
		case 'W':
			//调用方法清空消息栏
			game.ClearMessage();
			//设置英雄方向
			hero.setDirection(Up);
			//调用移动方法
			hero.move(map,hero,game, -1, 0);
			break;
		//下移
		case 's': 
		case 'S':
			game.ClearMessage();
			hero.setDirection(Down);
			hero.move(map, hero, game, 1, 0);
			break;
		//左移
		case 'a': 
		case 'A':
			game.ClearMessage();
			hero.setDirection(Left);
			hero.move(map, hero, game, 0, -1);
			break;
		//右移
		case 'd': 
		case 'D':
			game.ClearMessage();
			hero.setDirection(Right);
			hero.move(map, hero, game, 0, 1);
			break;
		//使用洞察之眼
		case 'l':
		case 'L':
			//判断英雄背包是否有洞察之眼
			if (hero.selectGoods(1001) >= 0)
			{
				//调用hero的detectionMonster()函数
				hero.detectionMonster(hero, map, game);
			}
			break;
		//重新开始
		case 'r':
		case 'R':
			//调用game的restart()函数
			game.restart(map,hero);	
			break;
		//退出游戏
		case 'q':
		case 'Q':
			//调用game的exitGame()函数
			game.exitGame();
			break;

		default:
			break;
	}
}
// 存储地图文件
void Map::saveFile(const char* filename)
{
	// 1.创建FILE的指针
	FILE* pfile = nullptr;

	// 2.打开文件,如果没有该文件,自动创建一个
	// fopen_s(&pfile,"map.txt", "wb+");//wb:write binary
	pfile = fopen(filename, "wb+");

	// 3.写入数据
	// 1.首地址 2.每一个数据的字节大小 3.数量 4.FILE*
	for (int i = 0; i < row; i++)
	{
		// 写入一行
		fwrite(pMap[i], sizeof(int), col, pfile);
	}

	// 4.关闭文件
	fclose(pfile);
}

// 读取文件
void Map::readFile(const char* filename)
{
	// 1.创建FILE的指针
	FILE* pfile = nullptr;

	// 2.打开文件,如果没有该文件,自动创建一个
	// fopen_s(&pfile,"map.txt", "rb+");//rb:read binary
	pfile = fopen(filename, "rb+");

	// 3.写入数据
	// 1.首地址 2.每一个数据的字节大小 3.数量 4.FILE*
	for (int i = 0; i < row; i++)
	{
		// 读取一行
		fread(pMap[i], sizeof(int),col, pfile);
	}

	// 4.关闭文件
	fclose(pfile);
}