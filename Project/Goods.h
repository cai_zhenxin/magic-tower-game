#pragma once

//物品类型的枚举
enum GoodsType
{
	ARTIFACT,				//神器
	QUESTS,					//任务道具
	KEY						//钥匙
};

struct Goods
{
	int id;					//编号
	char* name;				//名字
	GoodsType goodstype;	//物品类型
};


