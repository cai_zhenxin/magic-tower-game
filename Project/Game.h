#pragma once
class Hero;
class Map;
class Monster;

class Game
{
public:
	Game();
	~Game();

private:
	// n为当前层数
	int nowLevel;
	// 定义一个元素类型为int型的数组表示每一层地图是否玩过，0表示没有玩过，1表示已经玩过
	int isPlay[20];
	//定义一个bool型变量表示仙子是否已经与英雄对话过
	bool isTalk;
	//定义一个bool型变量表示冥灵魔王是否已经与英雄对话过
	bool isDemonTalk;
public:
	//设置当前层数
	void setNowLevel(int level);

	//设置当前层数
	void initIsPlay();

	//获取当前仙子是否已经跟英雄对话过的bool值
	bool getIsTalk();

	//设置当前仙子与英雄的对话状态
	void setIsTalk(bool b);

	//获取当前魔王是否已经跟英雄对话过的bool值
	bool getIsDemonTalk();

	//设置当前魔王与英雄的对话状态
	void setIsDemonTalk(bool b);

	//初始化方法
	void init(Map& map, Hero& hero);
		
	//绘制属性栏
	void drawTag(Map& map, Hero& hero);

	//绘制方法
	void draw(Map& map, Hero& hero);

	//切换地图
	void nextLevel(Map& map, Hero& hero, int x);

	//清除消息
	void ClearMessage();

	//绘制对话框
	void drawDialog(int x, int y, int width, int height, char *str);

	//在指定矩形区域输出文字
	void printText(int num, char* str);

	//在指定位置输出图片
	void printImage(int num, char* fileName);

	//定义一个函数计算当前英雄与某一怪物战斗所需要损耗的生命值
	char* calculateLoss(Hero hero, Monster monster);

	//定义一个函数判断数组中是否已经有某一元素
	bool isExist(int* monsterList, int id);

	//定义一个函数输出指定怪物的信息
	void monsterData(Monster monster, Hero& hero, int* monsterList, int& num, char* monsterNmae);

	//定义一个播放拾取物品音效的函数
	void playSound();

	//定义一个游戏UI界面函数
	void UI();

	//定义一个游戏说明界面函数
	void gameHelp();

	//判断点(x,y)是不是在按钮(bx,by)范围内
	bool judgeButton(int x, int y, int bx, int by);

	//定义一个重新开始游戏的功能函数
	void restart(Map& map, Hero& hero);

	//定义一个退出游戏的功能函数
	void exitGame();

	// 定义一个游戏结束界面函数
	void end(Hero& hero, Map& map);
};
