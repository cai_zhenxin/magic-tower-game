#pragma once
#include <graphics.h>

class Hero;
class Game;
#define ROW 13		//定义一个宏表示地图行数
#define COL 13		//定义一个宏表示地图列数
#define MAPNUM 50	//定义一个宏表示地图数量 



//地图类
class Map
{
public:
	Map(int** pMap,int row,int col);
	~Map();

private:
	int** pMap;	//地图的首地址
	int row;	//地图行数
	int col;	//地图列数

public:
	//获取地图首地址
	int** getPmap();

	//获取地图上指定坐标的数字
	int getPosNumber(Hero& hero, int x, int y);
	

	//设置地图上指定坐标的数字
	void setPosNumber(Hero& hero, int x, int y, int val);
	void setPosNumber(Hero& hero, int val);

	//获取内存
	void getMemory();

	//释放内存
	void deleteMemory();

	//获取英雄坐标
	void getHeroPos(Hero& hero);

	//初始化地图
	void initMap();

	//绘制地图
	void drawMap(Hero& hero);

	//实时更新
	void update(Map& map, Hero& hero, Game& game);

	//存储地图文件
	void saveFile(const char* filename);

	//读取地图文件
	void readFile(const char* filename);
};
