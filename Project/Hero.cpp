#pragma once
#include "Hero.h"
#include "Game.h"
#include "Goods.h"
#include "Monster.h"
#include "Map.h"
#include <graphics.h>
#include <iostream>
#include <conio.h>
#include <graphics.h>
#include "mmsystem.h"
#pragma comment(lib,"winmm.lib")
using namespace std;

//宏定义一个函数用于将变量名转换为字符串
#define GET_VARIABLE_NAME(Variable) #Variable
class Bag;

//定义一个物品数组
Goods allItems[10] =
{
	{ 1001, "洞察之眼", GoodsType::ARTIFACT },
	{ 1002, "幸运十字架", GoodsType::QUESTS },
	{ 1005, "红钥匙", GoodsType::KEY },
	{ 1006, "蓝钥匙",  GoodsType::KEY },
	{ 1007, "黄钥匙", GoodsType::KEY },
	{ 1008, "稀有钥匙", GoodsType::KEY }
};

//调用Monster类的带参构造函数定义并初始化所有怪物
Monster greenSlime(20, "绿头怪", 50, 20, 1, 1,1);
Monster redSlime(21, "红头怪", 70, 15, 2, 2,2);
Monster smallBat(22, "小蝙蝠", 100, 20, 5, 3,3);
Monster skeletonMan(23, "骷髅人", 110, 25, 5, 5,4);
Monster blackSlime(24, "青头怪", 200, 35, 10, 5,5);
Monster skeletonSoilder(25, "骷髅士兵", 150, 40, 20, 8,6);
Monster primaryMage(26, "初级法师", 125, 50, 25, 10,7);
Monster bigBat(27, "大蝙蝠", 150, 65, 30, 10, 8);
Monster beastMan(28, "兽面人", 300, 75, 45, 13, 10);
Monster skeletonCaptain(29, "骷髅队长", 400, 90, 50, 15, 12);
Monster stoneMan(48, "石头怪人", 500, 115, 65, 15, 15);
Monster sackclothMage(30, "麻衣法师", 250, 120, 70, 20, 17);
Monster primaryGuard(31, "初级卫兵", 450, 150, 90, 22, 19);
Monster redBat(32, "红蝙蝠", 550, 160, 90, 25, 20);
Monster hightMage(33, "高级法师", 100, 200, 110, 30, 25);
Monster monsterKing(34, "怪王", 700, 250, 125, 32, 30);
Monster whiteKnight(35, "白衣武士", 1300, 300, 150, 40, 35);
Monster goldGuard(36, "金卫士", 850, 350, 200, 45, 40);
Monster redMage(37, "红衣法师", 500, 400, 260, 47, 45);
Monster beastKnight(38, "兽面武士", 900, 450, 330, 50, 50);
Monster darkGuard(39, "冥卫兵", 1250, 500, 400, 55, 55);
Monster hightGuard(40, "高级卫兵", 1500, 560, 460, 60, 60);
Monster swordMan(41, "双手剑士", 1200, 620, 520, 65, 75);
Monster darkWarrior(42, "冥战士", 2000, 680, 590, 70, 65);
Monster goldCaptain(43, "金队长", 900, 750, 650, 77, 70);
Monster soulMage(44, "灵法师", 1500, 830, 730, 80, 70);
Monster darkCaptain(45, "冥队长", 2500, 900, 850, 84, 75);
Monster soulKnight(46, "灵武士", 1200, 980, 900, 88, 75);
Monster shadowWarrior(47, "影子战士", 3100, 1150, 1050, 92, 80);
Monster redDemon(80, "红衣魔王", 3100, 1150, 1050, 92, 80);
Monster demonKing(81, "冥灵魔王", 8000, 1800, 1500, 999, 999);

//带参构造函数
Hero::Hero(int Lv, int HP, int atk, int def, int money, int exp, int level, int posx, int posy, Direction direction)
{
	this->Lv = Lv;
	this->HP = HP;
	this->atk = atk;
	this->def = def;
	this->money = money;
	this->exp = exp;
	this->level = level;
	this->posx = posx;
	this->posy = posy;
	this->direction = direction;
	this->bag = bag;
}

Hero::~Hero()
{
}

//初始化英雄属性
void Hero::initHero()
{
	this->level = 0;
	this->Lv = 1;
	this->HP = 1000;
	this->atk = 10;
	this->def = 10;
	this->money = 0;
	this->exp = 0;
	this->bag.setGoodsNum(0);
	for (int i = 0; i < bag.getSize(); i++)
	{
		if (bag.getGoods(i) != nullptr)
		{
			delete bag.getGoods(i);
			bag.setNullptr(i);
		}
	}
}

//获取英雄等级
int Hero::getLv()
{
	return this->Lv;
}

//获取英雄血量
int Hero::getHP()
{
	return this->HP;
}

//获取英雄攻击力
int  Hero::getATK()
{
	return this->atk;
}

//获取英雄防御力
int  Hero::getDEF()
{
	return this->def;
}

//获取英雄所持金币数
int  Hero::getMoney()
{
	return this->money;
}

//获取当前英雄经验值
int Hero::getExp()
{
	return this->exp;
}

//获取英雄x坐标
int  Hero::getPosx()
{
	return this->posx;
}

//获取英雄y坐标
int  Hero::getPosy()
{
	return this->posy;
}

//获取英雄所在层数
int  Hero::getLevel()
{
	return this->level;
}

//获取英雄方向状态
Direction Hero::getDirection()
{
	return this->direction;
}

//获取英雄背包物品的内存
void Hero::getMemory(int i)
{
	this->bag.getMemory(i);
}

//使用物品后把对应的物品指针置空
void Hero::setNullptr(int i)
{
	this->bag.setNullptr(i);
}

//获取英雄背包
Bag Hero::getBag()
{
	return this->bag;
}

//修改x坐标
void Hero::setPosx(int x)
{
	this->posx = x;
}

//修改y坐标
void Hero::setPosy(int y)
{
	this->posy = y;
}

//修改英雄方向
void Hero::setDirection(Direction direction)
{
	this->direction = direction;
}

//修改英雄所在层数
void Hero::setLevel(int n)
{
	this->level = n;
}

//移动
void Hero::move(Map& map,Hero& hero,Game& game, int X, int Y)
{
	//设置消息栏的字体类型
	settextstyle(20, 10, "");
	//如果前面是空地、门、钥匙、增益物品、上楼梯、下楼梯
	if (0 == map.getPosNumber(hero, X, Y) ||
		3 == map.getPosNumber(hero, X, Y) ||
		4 == map.getPosNumber(hero, X, Y) ||
		5 == map.getPosNumber(hero, X, Y) ||
		13 == map.getPosNumber(hero, X, Y) ||
		14 == map.getPosNumber(hero, X, Y) ||
		15 == map.getPosNumber(hero, X, Y) ||
		16 == map.getPosNumber(hero, X, Y) ||
		17 == map.getPosNumber(hero, X, Y) ||
		10 == map.getPosNumber(hero, X, Y) ||
		11 == map.getPosNumber(hero, X, Y) ||
		50 == map.getPosNumber(hero, X, Y) ||
		51 == map.getPosNumber(hero, X, Y) ||
		52 == map.getPosNumber(hero, X, Y) ||
		53 == map.getPosNumber(hero, X, Y) ||
		54 == map.getPosNumber(hero, X, Y) ||
		55 == map.getPosNumber(hero, X, Y) || 
		56 == map.getPosNumber(hero, X, Y) || 
		57 == map.getPosNumber(hero, X, Y) ||
		58 == map.getPosNumber(hero, X, Y) || 
		59 == map.getPosNumber(hero, X, Y) ||
		60 == map.getPosNumber(hero, X, Y) ||
		61 == map.getPosNumber(hero, X, Y) ||
		62 == map.getPosNumber(hero, X, Y) ||
		63 == map.getPosNumber(hero, X, Y) ||
		64 == map.getPosNumber(hero, X, Y) ||
		65 == map.getPosNumber(hero, X, Y))
	{
		//如果前面为黄钥匙
		if (5 == map.getPosNumber(hero, X, Y))
		{
			//调用函数获取钥匙
			getGoods(1007);		
			outtextxy(100, 700, "你获得了一把黄钥匙！");
		}
		
		//如果前面为黄门
		if (15 == map.getPosNumber(hero, X, Y))
		{
			int pos = selectGoods(1007);
			if (pos >= 0)
			{
				hero.bag.setNullptr(pos);
				outtextxy(100, 700, "你使用了一把黄钥匙！");
			}
			else
			{
				outtextxy(100, 700, "你没有黄钥匙！");
				return;
			}
		}

		//如果前面为蓝钥匙
		if (4 == map.getPosNumber(hero, X, Y))
		{
			getGoods(1006);
			outtextxy(100, 700, "你获得了一把蓝钥匙！");
		}

		//如果前面为蓝门
		if (14 == map.getPosNumber(hero, X, Y))
		{
			int pos = selectGoods(1006);
			if (pos >= 0)
			{
				hero.bag.setNullptr(pos);
				outtextxy(100, 700, "你使用了一把蓝钥匙！");
			}
			else
			{
				outtextxy(100, 700, "你没有蓝钥匙！");
				return;
			}
		}

		//如果前面为红钥匙
		if (3 == map.getPosNumber(hero, X, Y))
		{
			getGoods(1005);
			outtextxy(100, 700, "你获得了一把红钥匙！");
		}

		//如果前面为红门
		if (13 == map.getPosNumber(hero, X, Y))
		{
			int pos = selectGoods(1005);
			if (pos >= 0)
			{
				hero.bag.setNullptr(pos);
				outtextxy(100, 700, "你使用了一把红钥匙！");
			}
			else
			{
				outtextxy(100, 700, "你没有红钥匙！");
				return;
			}
		}

		

		//如果前面为特质门
		if (16 == map.getPosNumber(hero, X, Y))
		{
			int pos = selectGoods(1008);
			if (pos >= 0)
			{
				hero.bag.setNullptr(pos);
				outtextxy(100, 700, "你使用了一把稀有钥匙！");
			}
			else
			{
				outtextxy(100, 700, "你没有稀有钥匙！");
				return;
			}
		}

		//如果前面为铁栏门
		if (17 == map.getPosNumber(hero, X, Y))
		{
			map.setPosNumber(hero, X, Y, 2); //前面的位置所代表的数字赋值为2
			map.setPosNumber(hero, 0); //英雄原来的位置所代表的数字赋值为0
			return;
		}

		//如果前面为上楼梯
		if (10 == map.getPosNumber(hero, X, Y))
		{
			game.nextLevel(map, hero, 1);
			return;
		}

		//如果前面为下楼梯
		if (11 == map.getPosNumber(hero, X, Y))
		{
			game.nextLevel(map, hero, -1);
			return;
		}

		//如果前面为红药水
		if (50 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			HP += 200;
			outtextxy(100, 700, "你增加了200点生命值！");
		}

		//如果前面为蓝药水
		if (51 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			HP += 500;
			outtextxy(100, 700, "你增加了500点生命值！");
		}

		//如果前面为红宝石
		if (52 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			atk += 3;
			outtextxy(100, 700, "你增加了3点攻击力！");
		}

		//如果前面为蓝宝石
		if (53 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			def += 3;
			outtextxy(100, 700, "你增加了3点防御力！");
		}
	
		
		//如果前面为铁剑
		if (54 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			atk += 10;
			outtextxy(100, 700, "你获得一把铁剑，增加10点攻击力！");
		}

		//如果前面为铁盾
		if (55 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			def += 10;
			outtextxy(100, 700, "你获得一件铁盾，增加10点防御力！");
		}

		//如果前面为钥匙盒
		if (56 == map.getPosNumber(hero, X, Y))
		{
			getGoods(1005);
			getGoods(1006);
			getGoods(1007);
			outtextxy(100, 700, "你捡到一个钥匙盒，获得了一把红钥匙，一把蓝钥匙，一把黄钥匙！");
		}

		//如果前面为钱袋
		if (57 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			money += 400;
			outtextxy(100, 700, "你捡到一个钱袋，获得了400个金币！");
		}

		//如果前面为六芒星
		if (58 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			Lv += 1;
			HP += 1000;
			atk += 10;
			def += 10;
			outtextxy(100, 700, "你吸收了一颗六芒星的力量，等级提升1级！");
		}

		//如果前面为洞察之眼
		if (59 == map.getPosNumber(hero, X, Y))
		{
			getGoods(1001);
			game.drawDialog(100, 200, 450, 150, "\n                  洞察之眼\n    上古神器，拥有该物品时，按L（l）可查看当前地图中怪物的基本信息，知己知彼，百战不殆！\n\n\n\n\n\n<按任意键继续>");
			_getch();
		}

		//如果前面为幸运十字架
		if (60 == map.getPosNumber(hero, X, Y))
		{
			//播放音效
			game.playSound();
			//获取物品
			getGoods(1002);
			//退出对话框
			game.drawDialog(100, 200, 450, 150, "\n                  幸运十字架\n    任务道具，将它交给仙子可以获得能力提升！\n\n\n\n\n\n<按任意键继续>");
			_getch();
		}

		//如果前面为朱雀剑
		if (61 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			atk += 70;
			outtextxy(100, 700, "你获得一把朱雀剑，增加70点攻击力！");
		}

		//如果前面为玄武盾
		if (62 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			def += 85;
			outtextxy(100, 700, "你获得一件玄武盾，增加70点防御力！");
		}

		//如果前面为圣光瓶
		if (63 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			HP *= 2;
			game.drawDialog(100, 200, 450, 150, "\n                  圣光瓶\n    上古神水，它可以让你的生命值翻倍！\n\n\n\n\n\n<按任意键继续>");
			_getch();
		}

		//如果前面为屠魔剑
		if (64 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			atk += 150;
			outtextxy(100, 700, "你获得一把屠魔剑，增加150点攻击力！");
		}

		//如果前面为镇魔盾
		if (65 == map.getPosNumber(hero, X, Y))
		{
			game.playSound();
			def += 190;
			outtextxy(100, 700, "你获得一件镇魔盾，增加190点防御力！");
		}

		map.setPosNumber(hero, X, Y,2); //前面的位置所代表的数字赋值为2
		map.setPosNumber(hero, 0); //英雄原来的位置所代表的数字赋值为0

	}
	//如果前面为绿头怪
	if (20 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map,greenSlime,game, X, Y);
	}

	//如果前面为红头怪
	if (21 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, redSlime, game, X, Y);
	}

	//如果前面为小蝙蝠
	if (22 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, smallBat, game, X, Y);
	}

	//如果前面为骷髅人
	if (23 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, skeletonMan, game, X, Y);
	}

	//如果前面为青头怪
	if (24 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, blackSlime, game, X, Y);
	}

	//如果前面为骷髅士兵
	if (25 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, skeletonSoilder, game, X, Y);
	}

	//如果前面为初级法师
	if (26 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, primaryMage, game, X, Y);
	}

	
	//如果前面为大蝙蝠
	if (27 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, bigBat, game, X, Y);
	}

	//如果前面为兽面人
	if (28 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, beastMan, game, X, Y);
	}

	//如果前面为骷髅队长
	if (29 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, skeletonCaptain, game, X, Y);
	}

	//如果前面为麻衣法师
	if (30 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, sackclothMage, game, X, Y);
	}
	
	//如果前面为初级卫兵
	if (31 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, primaryGuard, game, X, Y);
	}

	//如果前面为红蝙蝠
	if (32 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, redBat, game, X, Y);
	}

	//如果前面为高级法师
	if (33 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, hightMage, game, X, Y);
	}

	//如果前面为骷髅队长
	if (34 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, monsterKing, game, X, Y);
	}

	//如果前面为白衣武士
	if (35 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, whiteKnight, game, X, Y);
	}

	//如果前面为金卫士
	if (36 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, goldGuard, game, X, Y);
	}

	//如果前面为红衣法师
	if (37 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, redMage, game, X, Y);
	}

	//如果前面为兽面武士
	if (38 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, beastKnight, game, X, Y);
	}

	//如果前面为冥卫兵
	if (39 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, darkGuard, game, X, Y);
	}

	//如果前面为高级卫兵
	if (40 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, hightGuard, game, X, Y);
	}

	//如果前面为双手剑士
	if (41 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, swordMan, game, X, Y);
	}

	//如果前面为冥战士
	if (42 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, darkWarrior, game, X, Y);
	}

	//如果前面为金队长
	if (43 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, goldCaptain, game, X, Y);
	}

	//如果前面为灵法师
	if (44 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, soulMage, game, X, Y);
	}

	//如果前面为冥队长
	if (45 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, darkCaptain, game, X, Y);
	}

	//如果前面为灵武士
	if (46 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, soulKnight, game, X, Y);
	}

	//如果前面为影子战士
	if (47 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, shadowWarrior, game, X, Y);
	}

	//如果前面为石头怪人
	if (48 == map.getPosNumber(hero, X, Y))
	{
		meetMonster(map, stoneMan, game, X, Y);
	}

	//如果前面为红衣魔王
	if (80 == map.getPosNumber(hero, X, Y))
	{	
		meetMonster(map, redDemon, game, X, Y);
	}

	//如果前面为冥灵魔王
	if (81 == map.getPosNumber(hero, X, Y))
	{
		if (game.getIsDemonTalk() == 0)
		{
			game.drawDialog(100, 300, 450, 250, "\n勇士：\n万恶的魔头，你的死期到了！\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n冥灵魔王：\n哦？！没想到啊！居然有人类能闯到这里！\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n哼！少废话，快把公主交出来，不然就让你尝一下我宝剑的厉害！\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n冥灵魔王：\n哈哈哈哈哈~~~\n已经很久没有人敢跟我这样说话了，人类，你很不错，至少比死在你前面的人类都要强。\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n从今天开始，不会再有人命丧于此了，而你现在就要为你所做的一切付出代价！\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n冥灵魔王：\n哼！愚蠢的人类，口气倒是不小，就是不知道你的能力像不像你的口气一样大！想救公主？那就拔剑吧，我倒想看看你与之前的人类有何不同！\n\n<按任意键继续>");
			_getch();
			game.setIsDemonTalk(true);
		}
		meetMonster(map, demonKing, game, X, Y);
	}

	//如果前面为第二层的老头
	if (71 == map.getPosNumber(hero, X, Y) && 2 == hero.getLevel())
	{
		//绘制对话框
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n老头，你已经得救了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n另一个老头：\n噢！我的上帝！总算有人来救我了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n你是谁？怎么会被关在这里呢？\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n另一个老头：\n我是这附近的居民，你可以叫我西方不败，几个月前路过这座塔时不小心被抓进来了，太可怕了。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n原来如此，那你现在可以溜了，趁现在其他守卫还没有发觉，溜得越快越好！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n西方不败：\n溜是肯定要溜的，但是现在不急，我还没有报答你的救命之恩呢！年轻人，我看你骨骼惊奇，是练武的好料子，拜我为师怎么样，我可以传你绝世功法！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n谢谢你的好意，但是我现在还要去救公主，没那闲功夫拜师，以后再说吧！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n西方不败：\n好吧，既然这样，我就送你一把宝剑吧，这可是我年轻的时候用的，威力非凡，希望它可以助你一臂之力！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n那太好了！谢谢西方前辈，它肯定对我有很大帮助，你也快溜吧！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n西方不败：\n好的，那祝你好运，有机会见面再传你绝世功法，我先溜为敬！\n\n<按任意键继续>");
		_getch();
		map.getPmap()[11][8] = 0;
		atk += 30;
		outtextxy(100, 700, "你获得了一把钢剑，增加30点攻击力！");
	}

	//如果前面为第二层的商人
	if (70 == map.getPosNumber(hero, X, Y) && 2 == hero.getLevel())
	{
		//绘制对话框
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n老板，你已经得救了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n另一个商人：\n噢！我的上帝！总算有人来救我了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n你是谁？怎么会被关在这里呢？\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n另一个商人：\n我是这附近的商人，你可以叫我沈万四，几个月前路过这座塔时不小心被抓进来了，太可怕了。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n原来如此，那你现在可以溜了，趁现在其他守卫还没有发觉，溜得越快越好！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n沈万四：\n溜是肯定要溜的，但是现在不急，我还没有报答你的救命之恩呢！年轻人，你有没有要闲置物品，我可以高价收买！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n谢谢你的好意，但是我现在没有什么闲置物品，以后再说吧，我还要去救公主呢！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n沈万四：\n好吧，既然这样，我就送你一件护盾吧，这可是我不久前低价骗到...哦不，低价收购的宝贝，自己都舍不得用呢，希望它可以助你一臂之力！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n那太好了！谢谢沈老板，它肯定对我有很大帮助，你也快溜吧！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n沈万四：\n好的，那祝你好运，有机会见面的话你可以把不要的东西卖给我，价格公道，童叟无欺，我先溜了！\n\n<按任意键继续>");
		_getch();
		map.getPmap()[11][10] = 0;
		def += 30;
		outtextxy(100, 700, "你获得了一把钢盾，增加30点防御力！");
	}

	//如果前面为第五层的商人
	if (70 == map.getPosNumber(hero, X, Y) && 5 == hero.getLevel())
	{
		game.drawDialog(200, 100, 300, 500, "\n\n神秘商人：配钥匙！3块1把，10块3把，您配吗？(按下对应数字)：\n\n\n<1>购买1把黄钥匙（10金币）\n\n<2>购买1把蓝钥匙（50金币）\n\n<3>购买1把红钥匙（100金币）\n\n<4>下次一定");
		switch (_getch())
		{
		case '1':
			if (money >= 10)
			{
				money -= 10;
				getGoods(1007);
			}
			else
			{
				outtextxy(100, 700, "你的金币不足，去战斗可获得更多金币！");
			}
			break;
		case '2':
			if (money >= 50)
			{
				money -= 50;
				getGoods(1006);
			}
			else
			{
				outtextxy(100, 700, "你的金币不足，去战斗可获得更多金币！");
			}
			break;
		case '3':
			if (money >= 100)
			{
				money -= 100;
				getGoods(1005);
			}
			else
			{
				outtextxy(100, 700, "你的金币不足，去战斗可获得更多金币！");
			}
			break;
		case '4':
			break;

		}
	}

	

	//如果前面为第五层的老头
	if (71 == map.getPosNumber(hero, X, Y) && 5 == hero.getLevel())
	{
		game.drawDialog(200, 100, 300, 500, "\n\n神秘老头：你好，勇敢的年轻人！只要你有足够的经验，我就可以让你更加强大(按下对应数字)：\n\n\n<1>提升一级（需要100点）\n\n<2>攻击力+5（需要30点）\n\n<3>防御力+5（需要30点）\n\n<4>下次一定");
		switch (_getch())
		{
		case '1':
			if (exp >= 100)
			{
				exp -= 100;
				Lv += 1;
				HP += 1000;
				atk += 7;
				def += 7;
			}
			else
			{
				outtextxy(100, 700, "你的经验值不足，去战斗可获得更多经验！");
			}
			break;
		case '2':
			if (exp >= 30)
			{
				exp -= 30;
				atk += 5;
			}
			else
			{
				outtextxy(100, 700, "你的经验值不足，去战斗可获得更多经验！");
			}
			break;
		case '3':
			if (exp >= 30)
			{
				exp -= 30;
				def += 5;
			}
			else
			{
				outtextxy(100, 700, "你的经验值不足，去战斗可获得更多经验！");
			}
			break;
		case '4':
			break;

		}
	}

	//如果前面为小偷
	if (72 == map.getPosNumber(hero, X, Y))
	{
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n兄弟，你已经得救了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n小偷：\n噢！我的上帝！总算有人来救我了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n你是谁？怎么会被关在这里呢？\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n小偷：\n我是这附近大名鼎鼎的飞天怪盗肯基德，几个月前我混进这座塔来打探一下有没有什么宝贝，不料被守卫发现，这里的怪物太多了，我逃不出去，所以就被抓了。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n原来如此，那你现在可以溜了，趁现在其他守卫还没有发觉，溜得越快越好！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n小偷：\n溜是肯定要溜的，但是现在不急，我还没有报答你的救命之恩呢！好兄弟，你需要什么帮助吗？或许我能帮你！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n正好，我想问一下你有没有一种绿色特质材料门的钥匙，我想去第二层救其他人出来！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n小偷：\n噢，兄弟你太正直了，既然这样，我更不可能不帮忙了，之前我进来的时候就偷了一把绿色的钥匙，或许就是开二楼那扇门的，你可以拿去试一下！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n那太好了！谢谢你兄弟，我现在就去救人，你也快溜吧！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n小偷：\n好的，那祝你好运，有机会见面再请你喝酒，我先溜为敬！\n\n<按任意键继续>");
		_getch();
		map.getPmap()[1][6] = 0;
		getGoods(1008);
		outtextxy(100, 700, "你获得了一把稀有钥匙，该钥匙可以打开第二层的特质门！");
	}

	//如果前面为蓝商店
	if (73 == map.getPosNumber(hero, X, Y))
	{
		//判断英雄所持金币数是否大于等于25
		if (money >= 25)
		{
			game.drawDialog(200, 100, 300, 450, "\n\n低级商店：如果你可以给我25金币，我可以让你提升以下几种能力(按下对应数字)：\n\n\n<1>生命力+800\n\n<2>攻击力+4\n\n<3>防御力+4\n\n<4>下次一定");
			switch (_getch())
			{
				case '1':
					//英雄的金币数减少
					money -= 25;
					//英雄的血量增加
					HP += 800;
					break;
				case '2':
					money -= 25;
					//英雄的攻击力增加
					atk += 4;
					break;
				case '3':
					money -= 25;
					//英雄的防御力增加
					def += 4;
					break;
				case '4':
					break;			
			}
		}
		else
		{
			//绘制对话框
			game.drawDialog(200, 100, 300, 500, "\n\n\n\n\n  你不够25金币，请下次再来！\n\n\n\n<按任意键继续>");
			_getch();
		}
	}

	//如果前面为红商店
	if (74 == map.getPosNumber(hero, X, Y))
	{
		if (money >= 100)
		{
			game.drawDialog(200, 100, 300, 450, "\n\n高级商店：如果你可以给我100金币，我可以让你提升以下几种能力(按下对应数字)：\n\n\n<1>生命力+4000\n\n<2>攻击力+20\n\n<3>防御力+20\n\n<4>下次一定");
			switch (_getch())
			{
			case '1':
				money -= 100;
				HP += 4000;
				break;
			case '2':
				money -= 100;
				atk += 20;
				break;
			case '3':
				money -= 100;
				def += 20;
				break;
			case '4':
				break;

			}
		}
		else
		{
			game.drawDialog(200, 100, 300, 500, "\n\n\n\n\n  你不够100金币，请下次再来！\n\n\n\n<按任意键继续>");
			_getch();
		}
	}

	//如果前面为奸商
	if (75 == map.getPosNumber(hero, X, Y))
	{
		int pos;
		game.drawDialog(200, 100, 300, 500, "\n\n奸商：你好，我是奸商！我会以低价收购各种钥匙，如果你急需金币的话可以卖给我(按下对应数字)：\n\n\n<1>出售1把黄钥匙（7金币）\n\n<2>出售1把蓝钥匙（35金币）\n\n<3>出售1把红钥匙（70金币）\n\n<4>下次一定");
		switch (_getch())
		{
		case '1':
			pos = selectGoods(1007);
			if (pos >= 0)
			{
				money += 7;
				hero.bag.setNullptr(pos);
				outtextxy(100, 700, "你出售了一把黄钥匙！");
			}
			else
			{
				outtextxy(100, 700, "你没有黄钥匙！");
			}
			break;
		case '2':
			pos = selectGoods(1006);
			if (pos >= 0)
			{
				money += 35;
				hero.bag.setNullptr(pos);
				outtextxy(100, 700, "你出售了一把蓝钥匙！");
			}
			else
			{
				outtextxy(100, 700, "你没有蓝钥匙！");
			}
			break;
		case '3':
			pos = selectGoods(1005);
			if (pos >= 0)
			{
				money += 70;
				hero.bag.setNullptr(pos);
				outtextxy(100, 700, "你出售了一把红钥匙！");
			}
			else
			{
				outtextxy(100, 700, "你没有红钥匙！");
			}
			break;
		case '4':
			break;

		}
	}

	//如果前面为仙人
	if (76 == map.getPosNumber(hero, X, Y))
	{
		game.drawDialog(200, 100, 300, 500, "\n\n陆地真仙：你好，有缘人！只要你有足够的经验，我就可以让你更加强大(按下对应数字)：\n\n\n<1>提升三级（需要270点）\n\n<2>攻击力+17（需要95点）\n\n<3>防御力+17（需要95点）\n\n<4>下次一定");
		switch (_getch())
		{
		case '1':
			if (exp >= 270)
			{
				exp -= 270;
				Lv += 3;
				HP += 3000;
				atk += 21;
				def += 21;
			}
			else
			{
				outtextxy(100, 700, "你的经验值不足，去战斗可获得更多经验！");
			}
			break;
		case '2':
			if (exp >= 95)
			{
				exp -= 95;
				atk += 17;
			}
			else
			{
				outtextxy(100, 700, "你的经验值不足，去战斗可获得更多经验！");
			}
			break;
		case '3':
			if (exp >= 95)
			{
				exp -= 95;
				def += 17;
			}
			else
			{
				outtextxy(100, 700, "你的经验值不足，去战斗可获得更多经验！");
			}
			break;
		case '4':
			break;

		}
	}

	//如果前面为公主
	if (77 == map.getPosNumber(hero, X, Y))
	{
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....公主殿下，在下来救你了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n公主：\n哈利路亚！总算有人来救本公主了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n让公主殿下受惊了，您没有受伤吧？\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n公主：\n放心吧，英俊的勇士，那些怪物不敢把我怎么样！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n那就好，要是公主殿下有个三长两短，在下可不敢跟国外陛下交差！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n公主：\n你是父王派来救我的吗？\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n是的，国王陛下诏令天下勇士过来营救公主殿下。但是之前那些哥们可能已经命丧于此了，还好我来得及时，不然美丽的公主殿下就要...\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n公主：\n噢，英俊的勇士，那我先感谢你的救命之恩了。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n不用谢，这是在下对国王陛下的承诺！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n公主：\n放心吧！我回去之后一定会叫父王好好赏赐你的，英俊的勇士！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n这个之后再说，公主殿下，还请您速速随我出去吧，这个地方太危险了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n公主：\n好的，英俊的勇士，咱们走吧。\n\n<按任意键继续>");
		_getch();		
		outtextxy(100, 700, "恭喜你成功救出公主！");
		Sleep(2000);
		game.end(hero,map);
	}

	//如果前面为仙子
	if (79 == map.getPosNumber(hero, X, Y))
	{
		//判断英雄背包是否有任务道具
		if (selectGoods(1002) >= 0)
		{
			//绘制对话框
			game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n你需要的那个十字架我已经找到了！\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n仙子：\n真的吗？！太好了，谢谢你！\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n不用谢，那你现在可以赐予我力量了吗？我还记急着去救公主呢！\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n仙子：\n放心吧，我说到做到，你等我一下。\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n好的。\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n仙子：\n哈密哈密轰~~~~~~~\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n勇士：\n... ...\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n仙子：\n好了，我已经把我的一部分能力传给你了，加油，希望你可以成功救出公主。\n\n<按任意键继续>");
			_getch();
			game.drawDialog(100, 300, 450, 250, "\n勇士：\n谢谢你，我一定会好好利用这些力量的！\n\n<按任意键继续>");
			_getch();
			//消耗任务道具并提升英雄属性值
			bag.setNullptr(selectGoods(1002));
			HP += HP*0.333;
			atk += atk*0.333;
			def += def*0.333;
			outtextxy(100, 700, "你获得了仙子的力量，生命值、攻击力与防御力各提升30%！");
		}
		//判断英雄是否已经跟仙子对话过一次
		if (game.getIsTalk())
		{
			return;
		}
		//绘制对话框
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n仙子：\n你醒了！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n你是谁？我在哪？\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n仙子：\n我是这里的仙子，刚才你被小怪打晕了。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n.....\n剑，我的家传宝剑呢？！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n仙子：\n你的剑被抢走了！我只来得及救你。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n那，公主呢？我是来救公主的。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n仙子：\n公主还在里面，你这样进去是打不过那些小怪的。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n那我该怎么办，我答应国王一定要把公主就出来！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n仙子：\n放心吧！只要我把我的力量借给你，你就能打赢那些小怪了。不过，你得先帮我去找一样东西，找到了再来这里找我。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n找东西？找什么东西呢？\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n仙子：\n是一个十字架，中间有一颗红色的宝石。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n那个东西有什么用吗？\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n仙子：\n我本是这座塔的守护者，可就在不久前，从北方来了一批可恶的恶魔，他们占领了这座塔，并将我的魔力封印在了这个十字架里你找到它后，我就可以把我的力量借给你，你要记住：只有用我的魔力才可以打开二十一层的门。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n......\n好吧，我试试看。\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n仙子：\n我刚才去看过了，你的剑被放在三楼，你的盾在五楼，而那个十字架被放在七楼。要到七楼，你得先取回你的剑和盾。另外，在塔里的其他楼层还有一些存放了好几百年的宝剑和宝物，如果得到它们，对你会有很大的帮助！\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n勇士：\n......\n可是，我怎么进去呢？\n\n<按任意键继续>");
		_getch();
		game.drawDialog(100, 300, 450, 250, "\n仙子：\n我这里有三把钥匙，你先拿去，在塔里面还有很多这种钥匙，你务必要珍惜使用。勇敢地去闯吧，正义的勇士！\n\n<按任意键继续>");
		_getch();
		//获取钥匙
		getGoods(1005);
		getGoods(1006);
		getGoods(1007);
		outtextxy(100, 700, "你获得了一把红钥匙，一把蓝钥匙，一把黄钥匙！");
		game.setIsTalk(true);
	}

}

//声明一个判断背包中特定物品是否存在的函数
int Hero::selectGoods(int id)
{
	//遍历英雄背包
	for (int num = 0; num < this->getBag().getSize(); num++)
	{
		//如果当前元素为空指针
		if (this->getBag().getGoods(num) == nullptr)
		{
			//跳出本次循环
			continue;
		}
		//如果当前元素指向的id为所要查找的物品id
		if (this->getBag().getGoods(num)->id == id)
		{
			//返回数组下标
			return num;
		}
	}
	//如果背包中没有该物品则返回-1
	return -1;	
}

// 拾取物品
void Hero::getGoods(int id)
{
	//打开音乐文件
	mciSendString("open Music\\getGoods.wav", NULL, 0, NULL);
	//播放音乐文件	
	mciSendString("play Music\\getGoods.wav", NULL, 0, NULL);
	Sleep(300);
	//关闭音乐文件	
	mciSendString("close Music\\getGoods.wav", NULL, 0, NULL);
	Goods goods;
	// 先在全局数组中通过id查找对应的物品
	for (int i = 0; i < 10; i++)
	{
		if (id == allItems[i].id)
		{
			goods = allItems[i];
			break;
		}
	}

	// 先查找物品数组的第一个空位置
	for (int i = 0; i < this->getBag().getSize(); i++)
	{
		if (nullptr == this->getBag().getGoods(i))
		{
			// Goods*
			//this->getBag().setNullptr(i);
			this->getMemory(i);
			this->getBag().setGoods(i,goods) ;
			this->getBag().setGoodsNum(this->getBag().getGoodsNum()+1);
			break;
		}
	}
	
}

//定义战斗函数
bool Hero::battle(Monster monster,Game& game)
{
	int hurt_Monster = monster.getATK() - def;; 	//定义一个局部变量表示每一回合怪物对英雄造成的伤害
	if (monster.getATK() < def)    //如果怪物的攻击力小于英雄的防御力
	{
		hurt_Monster = 0;	//怪物伤害为0
	}

	int monsterHP = monster.getHP();
	int hurt_Hero = atk - monster.getDEF();		//定义一个局部变量表示每一回合英雄对怪物造成的伤害
	
	int time = 0;		//定义一个局部变量表示回合数
	while (monsterHP>0)		//当怪物的血量大于0时
	{
		monsterHP -= hurt_Hero;	//每一回合怪物的血量减少
		time++;		//回合数+1
	}
	if (HP <= hurt_Monster*time)	//预先判断英雄的血量是否足以战斗
	{
		char tag[50] = "不想死就别打！";
		outtextxy(100, 700, tag);
		return false;
	}
	else
	{
		//打开音乐文件
		mciSendString("open Music\\battle.wav", NULL, 0, NULL);
		//播放音乐文件	
		mciSendString("play Music\\battle.wav", NULL, 0, NULL);
		//Sleep(1000);
		
		monsterHP = monster.getHP();
		
		while (monsterHP>0)
		{
			//定义一个char型数组
			char str[1000];
			//设置矩形的填充色
			setfillcolor(BLACK);
			//设置填充面积
			fillrectangle(100, 150, 550, 550);
			//设置文字背景为透明
			setbkmode(TRANSPARENT);

			//定义并初始化一个矩形区域
			RECT r1 = { 140, 170, 530, 480 };
			//设置文字类型
			settextstyle(20, 10, _T(""));
			//设置文字颜色
			settextcolor(WHITE);
			HP -= hurt_Monster;
			monsterHP -= hurt_Hero;
			if (monsterHP < 0)
			{
				monsterHP = 0;
			}
			sprintf(str, "\n\n\n    英雄        VS        %s    \n\n\n    生命：%d          生命：%d    \n\n    攻击：%d           攻击：%d    \n\n    防御：%d           防御：%d    ", monster.getName(), HP, monsterHP, atk, monster.getATK(), def, monster.getDEF());
			//绘制文字
			drawtext(str, &r1, DT_WORDBREAK);
			Sleep(500);

		}
		//关闭音乐文件	
		mciSendString("close Music\\battle.wav", NULL, 0, NULL);
		//HP -= hurt_Monster*time;	//英雄的血量减少
		return true;
	}


}

//遇到怪物
void Hero::meetMonster(Map& map,Monster monster,Game& game, int X, int Y)
{
	if (atk <= monster.getDEF())		///如果英雄的攻击力小于怪物的防御力
	{
		char tag[50] = "对手过于强大，请提升实力之后再来！";
		outtextxy(100, 700, tag);
		return;		//跳出
	}	
	int hp_Init = HP;		//定义一个局部变量表示英雄战斗前的血量
	if (!battle(monster,game))	//如果英雄战斗之后将会死亡
	{
		return;
	}
	
	//英雄的金币增加
	money += monster.getMoney();
	//英雄的经验增加
	exp += monster.getExp();
	//编辑关于战斗结果的文字并输出
	char mesPK[100];
	sprintf(mesPK, "你消灭了一个%s，损失%d点血量，获得%d个金币，获得%d点经验！", monster.getName(), hp_Init - HP, monster.getMoney(),monster.getExp());
	outtextxy(100, 700, mesPK);
	map.getPmap()[posx + X][posy + Y] = 2; //前面的位置所代表的数字变成英雄所代表的数字
	map.getPmap()[posx][posy] = 0; //英雄原来的位置变成空地	
}

//使用洞察之眼
void Hero::detectionMonster(Hero& hero, Map& map, Game& game)
{
	//设置矩形的填充色
	setfillcolor(BLACK);
	//设置填充面积
	fillrectangle(0, 0,650, 650);
	//设置文字背景为透明
	setbkmode(TRANSPARENT);
	//定义一个变量表示当前地图的怪物种数
	int num = 0;	
	//定义一个int数组表示已检测过的怪物种类，避免重复检测
	int monsterType[10] = {0};
	//遍历地图
	for (int i = 1; i < 12; i++)
	{
		for (int j = 1; j < 12; j++)
		{
			switch (map.getPmap()[i][j])
			{
			//绿头怪
			case 20:
				if (!game.isExist(monsterType, 20))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(greenSlime);
					//调用函数输出该怪物的信息
					game.monsterData(greenSlime, hero, monsterType, num, monsterName);
				}			
				break;
			//红头怪
			case 21:
				if (!game.isExist(monsterType, 21))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(redSlime);
					//调用函数输出该怪物的信息
					game.monsterData(redSlime, hero, monsterType, num, monsterName);
				}
				break;
			//小蝙蝠
			case 22:
				if (!game.isExist(monsterType, 22))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(smallBat);
					//调用函数输出该怪物的信息
					game.monsterData(smallBat, hero, monsterType, num, monsterName);
				}
				break;
			//骷髅人
			case 23:
				if (!game.isExist(monsterType, 23))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(skeletonMan);
					//调用函数输出该怪物的信息
					game.monsterData(skeletonMan, hero, monsterType, num, monsterName);
				}
				break;
			//青头怪
			case 24:
				if (!game.isExist(monsterType, 24))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(blackSlime);
					//调用函数输出该怪物的信息
					game.monsterData(blackSlime, hero, monsterType, num, monsterName);
				}
				break;
			//骷髅士兵
			case 25:
				if (!game.isExist(monsterType, 25))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(skeletonSoilder);
					//调用函数输出该怪物的信息
					game.monsterData(skeletonSoilder, hero, monsterType, num, monsterName);
				}
				break;
			//初级法师
			case 26:
				if (!game.isExist(monsterType, 26))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(primaryMage);
					//调用函数输出该怪物的信息
					game.monsterData(primaryMage, hero, monsterType, num, monsterName);
				}
				break;
			//大蝙蝠
			case 27:
				if (!game.isExist(monsterType, 27))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(bigBat);
					//调用函数输出该怪物的信息
					game.monsterData(bigBat, hero, monsterType, num, monsterName);
				}
				break;
			//兽面人
			case 28:
				if (!game.isExist(monsterType, 28))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(beastMan);
					//调用函数输出该怪物的信息
					game.monsterData(beastMan, hero, monsterType, num, monsterName);
				}
				break;
			//骷髅队长
			case 29:
				if (!game.isExist(monsterType, 29))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(skeletonCaptain);
					//调用函数输出该怪物的信息
					game.monsterData(skeletonCaptain, hero, monsterType, num, monsterName);
				}
				break;
			//麻衣法师
			case 30:
				if (!game.isExist(monsterType, 30))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(sackclothMage);
					//调用函数输出该怪物的信息
					game.monsterData(sackclothMage, hero, monsterType, num, monsterName);
				}
				break;
			//初级卫兵
			case 31:
				if (!game.isExist(monsterType, 31))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(primaryGuard);
					//调用函数输出该怪物的信息
					game.monsterData(primaryGuard, hero, monsterType, num, monsterName);
				}
				break;
			//红蝙蝠
			case 32:
				if (!game.isExist(monsterType, 32))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(redBat);
					//调用函数输出该怪物的信息
					game.monsterData(redBat, hero, monsterType, num, monsterName);
				}
				break;
			//高级法师
			case 33:
				if (!game.isExist(monsterType, 33))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(hightMage);
					//调用函数输出该怪物的信息
					game.monsterData(hightMage, hero, monsterType, num, monsterName);
				}
				break;
			//怪王
			case 34:
				if (!game.isExist(monsterType, 34))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(monsterKing);
					//调用函数输出该怪物的信息
					game.monsterData(monsterKing, hero, monsterType, num, monsterName);
				}
				break;
			//白衣武士
			case 35:
				if (!game.isExist(monsterType, 35))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(whiteKnight);
					//调用函数输出该怪物的信息
					game.monsterData(whiteKnight, hero, monsterType, num, monsterName);
				}
				break;
		    //金卫士
			case 36:
				if (!game.isExist(monsterType, 36))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(goldGuard);
					//调用函数输出该怪物的信息
					game.monsterData(goldGuard, hero, monsterType, num, monsterName);
				}
				break;
			//红衣法师
			case 37:
				if (!game.isExist(monsterType, 37))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(redMage);
					//调用函数输出该怪物的信息
					game.monsterData(redMage, hero, monsterType, num, monsterName);
				}
				break;
			//兽面战士
			case 38:
				if (!game.isExist(monsterType, 38))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(beastKnight);
					//调用函数输出该怪物的信息
					game.monsterData(beastKnight, hero, monsterType, num, monsterName);
				}
				break;
			//冥卫兵
			case 39:
				if (!game.isExist(monsterType, 39))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(darkGuard);
					//调用函数输出该怪物的信息
					game.monsterData(darkGuard, hero, monsterType, num, monsterName);
				}
				break;
			//高级卫兵
			case 40:
				if (!game.isExist(monsterType, 40))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(hightGuard);
					//调用函数输出该怪物的信息
					game.monsterData(hightGuard, hero, monsterType, num, monsterName);
				}
				break;
			//双手剑士
			case 41:
				if (!game.isExist(monsterType, 41))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(swordMan);
					//调用函数输出该怪物的信息
					game.monsterData(swordMan, hero, monsterType, num, monsterName);
				}
				break;
			//双手剑士
			case 42:
				if (!game.isExist(monsterType, 42))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(darkWarrior);
					//调用函数输出该怪物的信息
					game.monsterData(darkWarrior, hero, monsterType, num, monsterName);
				}
				break;
			//金队长
			case 43:
				if (!game.isExist(monsterType, 43))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(goldCaptain);
					//调用函数输出该怪物的信息
					game.monsterData(goldCaptain, hero, monsterType, num, monsterName);
				}
				break;
			//灵法师
			case 44:
				if (!game.isExist(monsterType, 44))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(soulMage);
					//调用函数输出该怪物的信息
					game.monsterData(soulMage, hero, monsterType, num, monsterName);
				}
				break;
			//冥队长
			case 45:
				if (!game.isExist(monsterType, 45))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(darkCaptain);
					//调用函数输出该怪物的信息
					game.monsterData(darkCaptain, hero, monsterType, num, monsterName);
				}
				break;
			//灵武士
			case 46:
				if (!game.isExist(monsterType, 46))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(soulKnight);
					//调用函数输出该怪物的信息
					game.monsterData(soulKnight, hero, monsterType, num, monsterName);
				}
				break;
			//影子战士
			case 47:
				if (!game.isExist(monsterType, 47))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(shadowWarrior);
					//调用函数输出该怪物的信息
					game.monsterData(shadowWarrior, hero, monsterType, num, monsterName);
				}
				break;
			//石头怪人
			case 48:
				if (!game.isExist(monsterType, 48))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(stoneMan);
					//调用函数输出该怪物的信息
					game.monsterData(stoneMan, hero, monsterType, num, monsterName);
				}
				break;
				//红衣魔王
			case 80:
				if (!game.isExist(monsterType, 80))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(redDemon);
					//调用函数输出该怪物的信息
					game.monsterData(redDemon, hero, monsterType, num, monsterName);
				}
				break;
			case 81:
				if (!game.isExist(monsterType, 81))
				{
					//调用宏定义函数将怪物变量名转换成字符串
					char* monsterName = GET_VARIABLE_NAME(demonKing);
					//调用函数输出该怪物的信息
					game.monsterData(demonKing, hero, monsterType, num, monsterName);
				}
				break;
			default:
				break;
			}
		}
	}
	_getch();
	
}

