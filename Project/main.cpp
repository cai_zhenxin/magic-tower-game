//#include "Goods.h"
//#include "Monster.h"
#include "Hero.h"
#include "Game.h"
#include "Map.h"
#include <graphics.h>
#include "mmsystem.h"
#pragma comment(lib,"winmm.lib")
using namespace std;


int main()
{
	//定义一个Game类型变量
	Game game;
	//调用函数显示UI界面
	game.UI();
	//定义并初始化一个Map类型变量
	Map map(nullptr,13,13);
	//定义并初始化一个Hero类型的变量
	Hero hero(1, 1000, 10, 10, 0, 0, 0, 0, 0, Direction::Down);
	//调用gane的初始化函数	
	game.init(map, hero);   
	//map.saveFile("Map/map_14.txt");  //保存地图文件
	//播放背景音乐
	PlaySound(TEXT("Music/BGM.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);

	while (true)
	{
		game.draw(map, hero);				//绘制
		map.update(map, hero, game);		//更新
	}

	return 0;
}