#pragma once
//定义怪物类
class Monster
{
public:
	Monster(int num, char* name, int HP, int atk, int def, int money, int exp);
	~Monster();

private:
	int num;	//怪物编号
	char* name;	//怪物名字
	int HP;		//怪物血量
	int atk;	//怪物攻击力
	int def;	//怪物防御力
	int money;	//击杀获取的金币数
	int exp;	//击杀获取的经验值
public:
	//获取怪物编号
	int getID();
	//获取怪物名称
	char* getName();
	//获取怪物血量
	int getHP();
	//获取怪物攻击力
	int getATK();
	//获取怪物防御力
	int getDEF();
	//获取击杀金币数
	int getMoney();
	//获取击杀经验值
	int getExp();
};

