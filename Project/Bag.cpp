#pragma once
#include "Bag.h"

Bag::Bag()
{
	
	this->goodsNum = 0;
	this->size = 40;
	for (int i = 0; i < this->size; i++)
	{
		this->items[i] = nullptr;
	}
}

Bag::~Bag()
{

}

//获取物品指针
Goods* Bag::getGoods(int i)
{
	return this->items[i];
}

//将拾取的物品添加到背包中
void Bag::setGoods(int i,Goods goods)
{
	*(this->items[i]) = goods;
}

//设置物品
void Bag::setNullptr(int i)
{
	this->items[i] = nullptr;
}

void Bag::chageGoods(int i)
{
	this->items[i] = this->items[i + 1];
}

//获取英雄背包物品的内存
void Bag::getMemory(int i)
{
	this->items[i] = new Goods;
}


//设置物品数组首地址
void Bag::setPitems(int i,Goods* pGoods)
{
	this->items[i] = nullptr;
}

//获取背包容量
int Bag::getSize()
{
	return this->size;
}

//获取当前物品数量
int Bag::getGoodsNum()
{
	return this->goodsNum;
}

//设置背包容量
void Bag::setSize(int s)
{
	this->size = s;
}

//设置当前物品数量
void Bag::setGoodsNum(int num)
{
	this->goodsNum = num;
}